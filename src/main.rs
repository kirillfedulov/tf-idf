use std::env;
use std::fs;
use std::collections::HashMap;

use regex::Regex;

struct Document {
    file: String,

    terms: HashMap<String, u32>
}

fn main() {
    let mut args = env::args().skip(1);

    let search_term = args.next().expect("asdf");
    let files       = args.collect::<Vec<_>>();

    let mut documents: Vec<Document> = Vec::new();

    let re = Regex::new(r"\W+").unwrap();

    for file in &files {
        let contents = fs::read_to_string(file).expect(&format!("Failed to open {file}"));

        let mut document = Document{ file: file.to_string(), terms: HashMap::new() };

        for term in re.split(&contents) {
            document.terms.entry(term.to_string())
                .and_modify(|x| *x += 1).or_insert(1);
        }

        documents.push(document);
    }

    let d   = documents.iter().fold(0, |a, x| a + (x.terms.contains_key(&search_term) as u32));
    let idf = 1.0 + (documents.len() as f32 / (1.0 + d as f32)).ln();

    let mut sorted = Vec::new();

    for document in &documents {
        if let Some(&n) = document.terms.get(&search_term) {
            let tf = (n as f32) / (document.terms.values().sum::<u32>() as f32);

            sorted.push((document.file.to_string(), tf * idf));
        }
    }

    sorted.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap());
    
    for (file, tfidf) in &sorted {
        println!("For {} term '{}' is relevant by {}", file, search_term, tfidf);
    }
}
